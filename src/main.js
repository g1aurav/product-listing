import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import { Navbar } from 'bootstrap-vue/es/components';

Vue.use(BootstrapVue)
Vue.use(Navbar);

new Vue({
  el: '#app',
  render: h => h(App)
})
