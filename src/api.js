import axios from 'axios';

const errorHandler = function (error) {
  console.log(error);
};


const get = function (uri, headers = {}) {
  return axios.get(uri, headers)
    .catch(errorHandler);
};


export default {
  fetchData (data) {
    return get(`https://www.zopnow.com/toys/discounts.json?page=${data.page}&sort=${data.sort}`);
  }
};
